package com.handa.orderprocessingsystem.io.repository;

import com.handa.orderprocessingsystem.io.entity.UserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {

    UserEntity findByEmail(String id);

    UserEntity findByUserId(String id);

    UserEntity findUserByEmailVerificationToken(String token);
}
