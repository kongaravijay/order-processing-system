package com.handa.orderprocessingsystem.exceptions;

public class UserServiceException extends RuntimeException {

    public UserServiceException(String messege) {

        super(messege);
    }
}
