package com.handa.orderprocessingsystem.service;

import com.handa.orderprocessingsystem.dao.UserDao;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    UserDao createUser(UserDao user);

    UserDao getUserByUserId(String userId);

    UserDao getUser(String email);

    UserDao updateUser(String userId, UserDao user);

    void deleteUserByUserId(String userId);

    List<UserDao> getUsers(int page, int limit);

    boolean verifyEmailToken(String token);
}
