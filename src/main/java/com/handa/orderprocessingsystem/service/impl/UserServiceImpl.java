package com.handa.orderprocessingsystem.service.impl;

import com.handa.orderprocessingsystem.dao.AddressDao;
import com.handa.orderprocessingsystem.dao.UserDao;
import com.handa.orderprocessingsystem.io.entity.UserEntity;
import com.handa.orderprocessingsystem.io.repository.UserRepository;
import com.handa.orderprocessingsystem.service.UserService;
import com.handa.orderprocessingsystem.shared.Utils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    Utils utils;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDao createUser(UserDao user) {

        if (userRepository.findByEmail(user.getEmail()) != null) throw new RuntimeException("Record Already Exists");

        for (int i = 0; i < user.getAddressesList().size(); i++) {
            AddressDao addressDao = user.getAddressesList().get(i);
            addressDao.setUserDetails(user);
            addressDao.setAddressId(utils.generateAddressId(4));
            user.getAddressesList().set(i, addressDao);
        }
        ModelMapper modelMapper = new ModelMapper();
        UserEntity userEntity = modelMapper.map(user, UserEntity.class);

        userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        String publicUserId = utils.generateUserId(4);
        userEntity.setUserId(publicUserId);
        userEntity.setEmailVerificationToken(utils.generateEmailVerificationToken(publicUserId));
        userEntity.setEmailVerificationStatus(false);
        UserEntity storedUserDetails = userRepository.save(userEntity);

        UserDao returnValue = modelMapper.map(storedUserDetails, UserDao.class);
        return returnValue;
    }

    @Override
    public UserDao getUserByUserId(String userId) {
        UserDao returnValue = new UserDao();
        UserEntity userEntity = userRepository.findByUserId(userId);
        BeanUtils.copyProperties(userEntity, returnValue);

        return returnValue;
    }


    @Override
    public UserDao updateUser(String userId, UserDao user) {
        UserDao returnValue = new UserDao();
        UserEntity userEntity = userRepository.findByUserId(userId);
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());
        UserEntity updateddUserDetails = userRepository.save(userEntity);
        BeanUtils.copyProperties(updateddUserDetails, returnValue);
        return returnValue;
    }

    @Override
    public void deleteUserByUserId(String userId) {
        UserEntity userEntity = userRepository.findByUserId(userId);
        userRepository.delete(userEntity);
    }

    @Override
    public List<UserDao> getUsers(int page, int limit) {
        List<UserDao> returnValue = new ArrayList<>();
        Pageable pageableRequest = PageRequest.of(page, limit);
        Page<UserEntity> userPage = userRepository.findAll(pageableRequest);
        List<UserEntity> users = userPage.getContent();

        for (UserEntity userEntity : users) {
            UserDao userDao = new UserDao();
            BeanUtils.copyProperties(userEntity, userDao);
            returnValue.add(userDao);
        }
        return returnValue;
    }

    @Override
    public boolean verifyEmailToken(String token) {
        boolean isVerified = false;
        UserEntity userEntity = userRepository.findUserByEmailVerificationToken(token);

        if (Objects.nonNull(userEntity)) {

            boolean hasTokenExpired = Utils.hasTokenExpired(token);
            if (!hasTokenExpired) {
                userEntity.setEmailVerificationToken(token);
                userEntity.setEmailVerificationStatus(Boolean.TRUE);
                userRepository.save(userEntity);
                isVerified = true;
            }
        }
        return isVerified;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByEmail(email);
        if (Objects.isNull(userEntity)) throw new UsernameNotFoundException(email);

        return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(), userEntity.getEmailVerificationStatus(),
                true,
                true, true, new ArrayList<>());
        //return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(), new ArrayList<>());
    }

    @Override
    public UserDao getUser(String email) {
        UserEntity userEntity = userRepository.findByEmail(email);
        if (Objects.isNull(userEntity)) throw new UsernameNotFoundException(email);
        UserDao returnValue = new UserDao();
        BeanUtils.copyProperties(userEntity, returnValue);
        return returnValue;
    }
}

