package com.handa.orderprocessingsystem.controller;

/**
 * @author vkongara
 */
public enum RequestOperationName {
    DELETE, VERIFY_EMAIL
}
