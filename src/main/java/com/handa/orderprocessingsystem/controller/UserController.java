package com.handa.orderprocessingsystem.controller;

import com.handa.orderprocessingsystem.dao.UserDao;
import com.handa.orderprocessingsystem.exceptions.UserServiceException;
import com.handa.orderprocessingsystem.model.request.UserDetailRequestModel;
import com.handa.orderprocessingsystem.model.response.ErrorMessages;
import com.handa.orderprocessingsystem.model.response.OperationStatusModel;
import com.handa.orderprocessingsystem.model.response.RequestOperationStatus;
import com.handa.orderprocessingsystem.model.response.UserResponse;
import com.handa.orderprocessingsystem.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService userService;

    //private static final Logger LOGGER = new Logger(UserController.class);

    @PostMapping(value = "/createuser", consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}, //http://localhost:7070/users/createuser
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )
    public UserResponse createUser(@RequestBody UserDetailRequestModel userDetails) throws Exception {

        //LOGGER.info("====Create User Started=====");
        UserResponse returnValue = new UserResponse();
        if (userDetails.getFirstName().isEmpty())
            throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

        ModelMapper modelMapper = new ModelMapper();
        UserDao userDao = modelMapper.map(userDetails, UserDao.class);
        //BeanUtils.copyProperties(userDetails, userDao);
        UserDao createdUser = userService.createUser(userDao);
        //BeanUtils.copyProperties(createdUser, returnValue);
        returnValue = modelMapper.map(createdUser, UserResponse.class);
        return returnValue;
    }

    @GetMapping(path = "/getuser/{id}", consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )//http://localhost:8080/users/getuser/6m62
    public UserResponse getUser(@PathVariable String id) {
        UserResponse returnValue = new UserResponse();
        UserDao userDao = userService.getUserByUserId(id);
        BeanUtils.copyProperties(userDao, returnValue);
        return returnValue;
    }

    @GetMapping(path = "/getallusers", consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )//http://localhost:8080/users/getallusers?page=0&limit=5
    public List<UserResponse> getAllUsers(@RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "limit", defaultValue = "25") int limit) {
        List<UserResponse> returnValue = new ArrayList<>();
        List<UserDao> users = userService.getUsers(page, limit);
        for (UserDao userDao : users) {
            UserResponse userModel = new UserResponse();
            BeanUtils.copyProperties(userDao, userModel);
            returnValue.add(userModel);
        }
        return returnValue;
    }

    @PutMapping(path = "updateuser/{id}", consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )//http://localhost:8080/users/updateuser/qmig
    public UserResponse updateUser(@PathVariable String id, @RequestBody UserDetailRequestModel uderDetails) {
        UserResponse rerurnValue = new UserResponse();
        UserDao userDao = new UserDao();
        BeanUtils.copyProperties(uderDetails, userDao);
        UserDao updateUser = userService.updateUser(id, userDao);
        BeanUtils.copyProperties(updateUser, rerurnValue);
        return rerurnValue;
    }

    @DeleteMapping(path = "deleteuser/{id}", consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )//http://localhost:8080/users/deleteuser/kma0
    public OperationStatusModel deleteUser(@PathVariable String id) {
        OperationStatusModel returnValue = new OperationStatusModel();
        returnValue.setOperationName(RequestOperationName.DELETE.name());
        userService.deleteUserByUserId(id);
        returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
        return returnValue;
    }

    @GetMapping(path = "/email-verification")
    public OperationStatusModel verifyEmail(@RequestParam(value = "token") String token) {

        OperationStatusModel statusModel = new OperationStatusModel();
        statusModel.setOperationName(RequestOperationName.VERIFY_EMAIL.name());
        boolean verification = userService.verifyEmailToken(token);

        if (verification) {
            statusModel.setOperationResult(RequestOperationStatus.SUCCESS.name());
        } else {
            statusModel.setOperationResult(RequestOperationStatus.ERROR.name());
        }
        return statusModel;
    }

}
