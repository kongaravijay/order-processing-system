package com.handa.orderprocessingsystem.security;

import com.handa.orderprocessingsystem.SpringApplicationContext;

public class SecurityConstants {

        public static final long EXPIRATION_TIME = 864000000;
        public static final String TOKEN_PREFIX = "Bearer ";
        public static final String HEADER_STRING = "Authorization";
        public static final String SIGN_URL = "/users/createuser";
        public static final String TOKEN_SECRET = "JVDjavdsc10";
        public static final String H2_CONSOLE = "/h2-console/**";
        public static final String EMAIL_VERIFICATION_URL = "/users/email-verification";

        public static String getTokenSecret() {
                AppProperties appProperties = (AppProperties) SpringApplicationContext.getBean("AppProperties");
                return appProperties.getTokenSecret();
        }

}
