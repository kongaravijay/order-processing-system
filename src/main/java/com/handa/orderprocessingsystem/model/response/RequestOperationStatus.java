package com.handa.orderprocessingsystem.model.response;

/**
 * @author vkongara
 */
public enum RequestOperationStatus {
    ERROR, SUCCESS

}
